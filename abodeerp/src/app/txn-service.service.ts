import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database'
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TxnServiceService {

  /* properties  */

  transactions: Array<any>;
  
  constructor(private data: AngularFireDatabase) { }

  /* member functions */
  getTransaction(filter){
    return this.data.list('transactions').valueChanges().pipe(map( transaction=> {
      return transaction.filter(filter)
    }))
  }

  saveTransaction(tempTransaction: Transaction){
    this.data.list('transactions').push(tempTransaction);
  }
  
}

/* transaction class */
export class Transaction{
  amount:number;
  date:string;
  description:string;
  holding:string;
  paymentMode:string;
  source:string;
  transactionId:string;
  type:string;
}
