import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageuserComponent } from './manageuser/manageuser.component';
import { GledgerComponent } from './gledger/gledger.component';
import { IncomesComponent } from './incomes/incomes.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { BankncashComponent } from './bankncash/bankncash.component';

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent
  },
  {
    path: "manage/users",
    component: ManageuserComponent
  },
  {
    path: "ledger",
    component: GledgerComponent
  },
  {
    path: "income",
    component: IncomesComponent
  },
  {
    path: "expenses",
    component: ExpensesComponent
  },
  {
    path: "bankandcash",
    component: BankncashComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
