import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { AngularFireDatabase } from 'angularfire2/database';
@Injectable({
  providedIn: 'root'
})
export class BlockService {


  /* properties */
  blocks: {}[];

  constructor(private data: AngularFireDatabase) { }

  /* member functions */
  getBlocks(){
    return this.data.list('Blocks').snapshotChanges().pipe(map ( blockListItem =>{
      return blockListItem.map( blockItem => {
        const block_name = blockItem.key;
        const block_details = blockItem.payload.val();
        return {block_name, block_details}
      })
    }));
  }
}
