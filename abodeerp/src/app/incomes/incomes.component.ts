/* for now this class is written to show all the credits. later on a filter will be added to display only monthly inomes */
import { Component, OnInit } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Transaction, TxnServiceService } from '../txn-service.service';

@Component({
  selector: 'app-incomes',
  templateUrl: './incomes.component.html',
  styleUrls: ['./incomes.component.css']
})
export class IncomesComponent implements OnInit {

  transactions: Array<any>;
  constructor(private txnService: TxnServiceService) { }

  ngOnInit() {
    this.txnService.getTransaction(this.checkIfCredit).subscribe( transactions => {
      console.log(transactions);
      this.transactions = transactions;
      });
  }

  checkIfCredit(transaction: Transaction){
   // console.log('inside type filter');
    if(transaction.type == "Credit")
      return transaction;
  }
}
