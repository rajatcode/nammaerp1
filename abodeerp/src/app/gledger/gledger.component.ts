import { Component, OnInit } from '@angular/core';
import { TxnServiceService, Transaction } from '../txn-service.service';

@Component({
  selector: 'app-gledger',
  templateUrl: './gledger.component.html',
  styleUrls: ['./gledger.component.css']
})
export class GledgerComponent implements OnInit {

  transactions: Array<any>;
  constructor(private txnService: TxnServiceService) { }

  ngOnInit() {
    this.txnService.getTransaction(this.checkIfCredit).subscribe( transactions => {
      console.log(transactions);
      this.transactions = transactions;
      });
  }

  checkIfCredit(transaction: Transaction){
   // console.log('inside type filter');
    
      return transaction;
  }

}
