import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankncashComponent } from './bankncash.component';

describe('BankncashComponent', () => {
  let component: BankncashComponent;
  let fixture: ComponentFixture<BankncashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankncashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankncashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
