import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  /* properties */

  holdings: {}[];

  constructor(private data: AngularFireDatabase) { }

  getHoldings(){
    return this.data.list('transaction-holdings').snapshotChanges().pipe(map ( holdingListItem =>{
      return holdingListItem.map( blockItem => {
        const holding_name = blockItem.key;
        const holding_value = blockItem.payload.val();
        return {holding_name, holding_value}
      })
    }));
  }

  updateTxnId(newTxn: any){
    //console.log(newTxn);
    
    this.data.object('IdKeeper/').update({lastTxnId: newTxn});
    
  }

  updateCurrentTxnId(){
    let newTxnId: number;
    this.data.object('IdKeeper/lastTxnId').valueChanges().subscribe( txnId => { 
//      console.log(txnId);

      newTxnId = parseInt(txnId+"")});
      return newTxnId;
  }
}
