import { TestBed } from '@angular/core/testing';

import { TxnServiceService } from './txn-service.service';

describe('TxnServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TxnServiceService = TestBed.get(TxnServiceService);
    expect(service).toBeTruthy();
  });
});
