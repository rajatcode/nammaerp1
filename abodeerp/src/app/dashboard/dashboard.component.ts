import { Component, OnInit } from '@angular/core';
import { TxnServiceService } from '../txn-service.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  /* properties */
  netBalance: number;
  
  constructor(private txnService: TxnServiceService) { }

  ngOnInit() {
    let sum: number = 0;
    this.txnService.getTransaction(filter => {
      return filter;
    }).subscribe( (transactions:Array<any>) => {
      transactions.forEach( transaction => {
        let amt: number = parseInt(transaction.amount+"");
        //console.log(amt);
        if(transaction.type == "Credit"){        
          sum = sum + amt;
        }else if(transaction.type == "Debit"){
          sum = sum - amt;
        }
      });
      console.log(sum);
      this.netBalance = sum;
    }
    ) 
    
    
  }

}
