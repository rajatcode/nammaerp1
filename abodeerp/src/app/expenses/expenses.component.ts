import { Component, OnInit } from '@angular/core';
import { TxnServiceService, Transaction } from '../txn-service.service';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {

  transactions: Array<any>;
  constructor(private txnService: TxnServiceService) { }

  ngOnInit() {
    this.txnService.getTransaction(this.checkIfCredit).subscribe( transactions => {
      console.log(transactions);
      this.transactions = transactions;
      });
  }

  checkIfCredit(transaction: Transaction){
   // console.log('inside type filter');
    if(transaction.type == "Debit")
      return transaction;
  }

}
