import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigatorComponent } from './navigator/navigator.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManageuserComponent } from './manageuser/manageuser.component';
import { GledgerComponent } from './gledger/gledger.component';
import { IncomesComponent } from './incomes/incomes.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { BankncashComponent } from './bankncash/bankncash.component';
import { environment } from 'src/environments/environment';

import { ReactiveFormsModule } from '@angular/forms';
import { from } from 'rxjs';
import { TransactionFormModalComponent } from './transaction-form-modal/transaction-form-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigatorComponent,
    DashboardComponent,
    ManageuserComponent,
    GledgerComponent,
    IncomesComponent,
    ExpensesComponent,
    BankncashComponent,
    TransactionFormModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, 'AbodeERP'),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
