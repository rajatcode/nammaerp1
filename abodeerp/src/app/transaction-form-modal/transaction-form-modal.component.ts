import { Component, OnInit } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { FormControl, FormGroup } from '@angular/forms';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { formatNumber } from '@angular/common';
import { Transaction, TxnServiceService } from '../txn-service.service';
import { BlockService } from '../block.service';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-transaction-form-modal',
  templateUrl: './transaction-form-modal.component.html',
  styleUrls: ['./transaction-form-modal.component.css']
})
export class TransactionFormModalComponent implements OnInit {

  /*  property declaration */
  transactions: Array<any>;
  blocks: {}[];
  holdings: {}[];
  lastTransactionId: number;
  profileForm = new FormGroup({
    amount: new FormControl(''),
    description: new FormControl(''),
    holding: new FormControl(''),
    block: new FormControl(''),
    flat_no: new FormControl(''),
    type: new FormControl(''),
    paymentMode: new FormControl(''),
    date: new FormControl(''),

  });

  /* const */
  constructor(private blockSvc: BlockService, private categorySvc: CategoryService, private txnService: TxnServiceService) { }
 
  
  /* member function starts here */

  ngOnInit() {
     
    this.lastTransactionId = this.categorySvc.updateCurrentTxnId();
   
    this.blockSvc.getBlocks().subscribe( blockDetails => {
      console.log(blockDetails);  
      this.blocks = blockDetails;
    })

    this.categorySvc.getHoldings().subscribe( holdingDetails => {
      console.log(holdingDetails);
      this.holdings = holdingDetails;
    })

  }


    onSubmit(){
      
      //console.log('form submitted');
      //console.log(this.profileForm.value);
      //console.log(this.profileForm.controls.amount.value);
      
      //creating and mapping the transaction to the form data
      let tempTransaction:Transaction  = new Transaction(); 

      tempTransaction.amount = this.profileForm.controls.amount.value;
      tempTransaction.date = this.profileForm.controls.date.value;
      tempTransaction.description = this.profileForm.controls.description.value;
      tempTransaction.holding = this.profileForm.controls.holding.value;
      tempTransaction.paymentMode = this.profileForm.controls.paymentMode.value;
      tempTransaction.source = this.profileForm.controls.block.value + "/" + this.profileForm.controls.flat_no.value;
      tempTransaction.transactionId = "txn_" + formatNumber(this.lastTransactionId + 1, "en");
      tempTransaction.type = this.profileForm.controls.type.value;
      
      console.log(tempTransaction.transactionId);

      this.txnService.saveTransaction(tempTransaction);

      this.lastTransactionId = this.categorySvc.updateCurrentTxnId();
      this.categorySvc.updateTxnId(tempTransaction.transactionId);
      
    }

}

